package infobalt.robusta.repositories;

import java.util.List;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Project;

public interface AssignmentRepository {

	public void save(Assignment newAssignment);

	public void delete(Assignment assignment);

	public List<Assignment> findAll();

	public List<Assignment> findAllByProject(Project currentProject);

}
