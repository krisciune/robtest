package infobalt.robusta.repositories;

import java.util.List;

import infobalt.robusta.entities.Project;

public interface ProjectRepository {

	void save(Project newProject);

	void delete(Project project);

	List<Project> findAll();

	Project findById(Long id);

}
