package infobalt.robusta.repositoriesJPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.AssignmentRepository;
import infobalt.robusta.entities.Project_;
import infobalt.robusta.entities.Assignment_;

public class AssignmentRepositoryJPA implements AssignmentRepository {

	static final Logger log = LoggerFactory.getLogger(ProjectRepositoryJPA.class);

	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}
	
	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	@Override
	public void save(Assignment assignment) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(assignment) && assignment.getId() != null) {
				assignment = entityManager.merge(assignment);
			}
			entityManager.persist(assignment);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}

	}

	@Override
	public void delete(Assignment assignment) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			assignment = entityManager.merge(assignment);
			entityManager.remove(assignment);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Assignment> findAllByProject(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			project = entityManager.merge(project);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Assignment> cq = cb.createQuery(Assignment.class);
			Root<Assignment> root = cq.from(Assignment.class);
			cq.select(root);
			cq.where(cb.equal(root.get(Assignment_.project), project));
			TypedQuery<Assignment> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

}
