package infobalt.robusta.repositoriesJPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.ProjectRepository;
import infobalt.robusta.entities.Project_;

public class ProjectRepositoryJPA implements ProjectRepository {

	static final Logger log = LoggerFactory.getLogger(ProjectRepositoryJPA.class);

	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}
	
	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	@Override
	public void save(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(project))
				project = entityManager.merge(project);
			entityManager.persist(project);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}

	}

	@Override
	public void delete(Project project) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			project = entityManager.merge(project);
			entityManager.remove(project);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}

	}

	@Override
	public List<Project> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Project> cq = cb.createQuery(Project.class);
			Root<Project> root = cq.from(Project.class);
			cq.select(root);
			TypedQuery<Project> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Project findById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Project> cq = cb.createQuery(Project.class);
			Root<Project> root = cq.from(Project.class);
			cq.where(cb.equal(root.get(Project_.id), id));
			TypedQuery<Project> q = entityManager.createQuery(cq);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}


}
