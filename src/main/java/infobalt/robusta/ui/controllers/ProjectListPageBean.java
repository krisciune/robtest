package infobalt.robusta.ui.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.ProjectRepository;

/**
 * This class is the Invoice list page controller. It deals with user commands
 * and navigation
 *
 */
public class ProjectListPageBean implements Serializable { // this is the
															// Controller,
															// scope: request

	/**
	 * 
	 */
	private static final long serialVersionUID = -8143077418240345336L;

	static final Logger log = LoggerFactory.getLogger(ProjectListPageBean.class);

	public static final String NAV_LIST_PROJECTS = "list-projects";
	public static final String NAV_UPDATE_PROJECT = "update-project";
	public static final String NAV_UPDATE_ASSIGNMENTS = "update-assignments";

	/**
	 * this class holds data in session
	 */
	public static class ProjectListPageData implements Serializable { // this is
																	// the
																	// Model,
																	// scope:
																	// session

		/**
		 * 
		 */
		private static final long serialVersionUID = 5040310448257955354L;

		@Valid
		private Project currentProject;

		public void init() {
			currentProject = new Project();
		}

		public Project getCurrentProject() {
			return currentProject;
		}

		public void setCurrentProject(Project currentProject) {
			this.currentProject = currentProject;
		}

	}

	private ProjectListPageData projectListPageData; // the model, session scope
	private ProjectRepository projectRepo; // singleton

	public String create() {
		projectListPageData.setCurrentProject(new Project());
		return NAV_LIST_PROJECTS;
	}

	public String delete(Project project) {
		projectRepo.delete(project);
		return NAV_LIST_PROJECTS;
	}

	public List<Project> getProjectList() {
		return projectRepo.findAll();
	}
	
	public List<Assignment> findAssignmentsByProject() {
		return projectListPageData.getCurrentProject().getAssignments();
	}
	
	public List<Assignment> getFreshAssignmentList() {
		return projectRepo.findById(projectListPageData.getCurrentProject().getId()).getAssignments();
	}

	public String update(Project project) {
		projectListPageData.setCurrentProject(project);
		return NAV_UPDATE_PROJECT;
	}

	public String save() {
		projectRepo.save(projectListPageData.getCurrentProject());
		return NAV_LIST_PROJECTS;
	}
	
	public String cancel() {
		projectListPageData.setCurrentProject(new Project());
		return NAV_LIST_PROJECTS;
	}

	public ProjectListPageData getProjectListPageData() {
		return projectListPageData;
	}

	public void setProjectListPageData(ProjectListPageData projectListPageData) {
		this.projectListPageData = projectListPageData;
	}

	public ProjectRepository getProjectRepo() {
		return projectRepo;
	}

	public void setProjectRepo(ProjectRepository projectRepo) {
		this.projectRepo = projectRepo;
	}

}
