package infobalt.robusta.ui.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import infobalt.robusta.entities.Assignment;
import infobalt.robusta.entities.Project;
import infobalt.robusta.repositories.AssignmentRepository;
import infobalt.robusta.repositories.ProjectRepository;
import infobalt.robusta.ui.controllers.ProjectListPageBean.ProjectListPageData;

/**
 * This class is the Invoice list page controller. It deals with user commands and
 * navigation. Scope: request
 *
 */
public class AssignmentListPageBean implements Serializable { // this is the Controller
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1389684115871062390L;

	static final Logger log = LoggerFactory.getLogger(AssignmentListPageBean.class);
	
	public static final String NAV_LIST_PROJECTS = "list-projects";
	public static final String NAV_UPDATE_PROJECT = "update-project";
	
	/**
	 * this class holds data in session and does nothing else whatsoever; scope: session
	 */
	public static class AssignmentListPageData implements Serializable {  // this is the Model

		/**
		 * 
		 */
		private static final long serialVersionUID = -8131673305859876205L;

		@Valid
		private Assignment currentAssignment;
		
		public void init() {
			currentAssignment = new Assignment();
		}

		public Assignment getCurrentAssignment() {
			return currentAssignment;
		}

		public void setCurrentAssignment(Assignment currentAssignment) {
			this.currentAssignment = currentAssignment;
		}
	}

	private ProjectRepository projectRepo;				// singleton
	private ProjectListPageData projectListPageData;	// model: session scope
	private ProjectListPageBean projectListPage;	// controller: request scope
	private AssignmentRepository assignmentRepo;			// singleton
	private AssignmentListPageData assignmentListPageData;// model: session scope


	public String addNewAssignment(Project currentProject) {
		assignmentListPageData.setCurrentAssignment(new Assignment());
		projectListPageData.setCurrentProject(currentProject);
		return ProjectListPageBean.NAV_UPDATE_ASSIGNMENTS;
	}
	
	public String update(Assignment assignment) {
		assignmentListPageData.setCurrentAssignment(assignment);
		return ProjectListPageBean.NAV_UPDATE_ASSIGNMENTS;
	}
	
	public String save() {
		assignmentListPageData.getCurrentAssignment().setProject(projectListPageData.getCurrentProject());
		assignmentRepo.save(assignmentListPageData.getCurrentAssignment());
		assignmentListPageData.setCurrentAssignment(new Assignment());
		return ProjectListPageBean.NAV_UPDATE_ASSIGNMENTS;
	}
	
	public String delete(Assignment assignment) {
		assignmentRepo.delete(assignment);
		return ProjectListPageBean.NAV_UPDATE_ASSIGNMENTS;
	}
	
	public String cancel() {
		assignmentListPageData.setCurrentAssignment(new Assignment());
		return ProjectListPageBean.NAV_UPDATE_ASSIGNMENTS;
	}
	
	public List<Assignment> findAssignmentsByProject() {
		return assignmentRepo.findAllByProject(projectListPage.getProjectListPageData().getCurrentProject());
	}

	public ProjectListPageData getProjectListPageData() {
		return projectListPageData;
	}
	public void setProjectListPageData(ProjectListPageData projectListPageData) {
		this.projectListPageData = projectListPageData;
	}
	public AssignmentListPageData getAssignmentListPageData() {
		return assignmentListPageData;
	}
	public void setAssignmentListPageData(AssignmentListPageData assignmentListPageData) {
		this.assignmentListPageData = assignmentListPageData;
	}

	public ProjectRepository getProjectRepo() {
		return projectRepo;
	}

	public void setProjectRepo(ProjectRepository projectRepo) {
		this.projectRepo = projectRepo;
	}

	public ProjectListPageBean getProjectListPage() {
		return projectListPage;
	}

	public void setProjectListPage(ProjectListPageBean projectListPage) {
		this.projectListPage = projectListPage;
	}

	public AssignmentRepository getAssignmentRepo() {
		return assignmentRepo;
	}

	public void setAssignmentRepo(AssignmentRepository assignmentRepo) {
		this.assignmentRepo = assignmentRepo;
	}

	
}
